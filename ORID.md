## O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

1. Learned what Tasking is, including the details to pay attention to when task splitting and how to split tasks.

2. After learning Context Map, use Context Map to draw the corresponding task after Tasking.

3. Participated in the group discussion and carried out several exercises about Tasking and Context Map.

## R (Reflective): Please use one word to express your feelings about today's class.

    Fruitful.

## I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

    Through the way of learning while typing code can deepen our impression, so that students are more involved in this process.

## D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

    The idea that the Tasking you learned today can be applied in software development is very important.
